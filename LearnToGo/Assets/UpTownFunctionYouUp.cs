﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpTownFunctionYouUp : MonoBehaviour {

	int health      = 100;
	int attackpower = 25;
	bool shieldOn   = true;
	int shieldAmt   = 15;

	public Button shield;
	public Text healthText;

	void Start(){
		Debug.Log ("Health at start: " + health);
		shield.GetComponentInChildren<Text> ().text = "Shield On";
		shield.GetComponent<Image> ().color = Color.green;
		healthText.text = " " + health + " ";
	}

	void Update(){
		healthText.text = " " + health + " ";
	}

	public void Attack(){
		int damageToInflict = getAttackDamage (shieldOn, shieldAmt, attackpower);
		health -= damageToInflict;
		Debug.Log ("Health after attact: " + health);
	}

	public void Heal(){
		int healAmount = GetRandomNumber ();
		health += healAmount;
		Debug.Log ("Received " + healAmount + " health");
		Debug.Log ("You now have " + health + " health");
	}

	private int getAttackDamage(bool isShieldOn, int theShieldAnmt, int theAttackpower){
		int damage = 0;

		if (isShieldOn) {
			damage = theAttackpower - (int)((float)theShieldAnmt - 0.10f);
		} else {
			damage = theAttackpower;
		}

		return damage;
	}

	public void shieldBtn(){
		if (shieldOn) {
			shieldOn = false;
			shield.GetComponent<Image> ().color = Color.red;
			shield.GetComponentInChildren<Text> ().text = "Shield Off";
		} else {
			shieldOn = true;
			shield.GetComponent<Image> ().color = Color.green;
			shield.GetComponentInChildren<Text> ().text = "Shield On";
		}
	}

	private int GetRandomNumber(){
		return Random.Range (2, 10);
	}
}
