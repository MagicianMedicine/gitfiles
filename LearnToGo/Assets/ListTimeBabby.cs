﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListTimeBabby : MonoBehaviour {

	public GameObject cubePrefab;
	GameObject[] cubes = new GameObject[5];
	Color[] colors = { Color.red, Color.blue, Color.yellow, Color.black, Color.green };


	float spacer = 0.5f;

	// Use this for initialization
	void Start () {
		for(int x = 0; x < cubes.Length; x++){
			GameObject cube = Instantiate (cubePrefab);
			cube.transform.position = new Vector3 (x + (spacer * x), cube.transform.position.y); 
			cube.GetComponent<Renderer> ().material.color = colors [x];
			cubes [x] = cube;

		}


	}
	// Update is called once per frame
	void Update () {
		
	}
}
